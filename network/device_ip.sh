#!/bin/sh

set -e

inventory_device_ip=$1

if [ -z "$inventory_device_ip" ]; then
	echo "No device IP specified"
	echo "Usage:\t $0 <device_ip>"
	exit 1
fi


if [ "$LAVA" = "y" ]; then
	alias test-result='lava-test-case'
	alias test-exception='lava-test-raise'
else
	alias test-result='echo'
	alias test-exception='echo'
fi

actual_device_ip=`/sbin/ifconfig |awk -F" " '$1 ~ /^inet$/ && $2 !~ /^127\.0\.0\.1$/ {print $2}'|head -1`

if [ "z$actual_device_ip" = "z$inventory_device_ip" ]
then
	test-result network-device-ip --result pass --measurement "$actual_device_ip"
else
	test-result network-device-ip --result fail
	echo "Inventory device_ip '$inventory_device_ip' not match '$actual_device_ip' address on device"
fi
