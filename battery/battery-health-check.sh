#!/bin/sh

set -e

threshold=$1

[ -z "$threshold" ] && {
    echo "No threshold provided"
    echo "Usage: battery-health-check.sh <battery-threshold>"
    exit 1
}

if [ "$LAVA" = "y" ]; then
    alias test-result='lava-test-case'
    alias test-exception='lava-test-raise'
else
    alias test-result='echo'
    alias test-exception='echo'
fi

if battery=$(grep -l Battery /sys/class/power_supply/*/type); then
    test-result battery-present --result pass
else
    test-result battery-present --result fail
    echo "No battery detected"
    exit 1
fi

if cap=$(cat "${battery%/type}"/capacity); then
    if [ "$cap" -gt "$threshold" ]; then
        test-result battery-capacity --result pass --measurement "$cap"
    else
        test-exception "Battery capacity under $threshold% ($cap%); job exit"
    fi
else
    test-result battery-capacity --result fail
fi