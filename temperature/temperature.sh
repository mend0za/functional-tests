#!/bin/sh

set -e

if [ "$LAVA" = "y" ]; then
    alias test-result='lava-test-case'
else
    alias test-result='echo'
fi

if zones=$(find /sys/class/thermal/thermal_zone*); then
    test-result temperature-sensors-present --result pass
else
    test-result temperature-sensors-present --result fail
    echo "No temperature sensors detected"
    exit 1
fi

for zone in $zones; do
    temp=$(cat "$zone"/temp) && res='pass' || res='fail'
    test-result "$(cat "$zone"/type)" --result "$res" --measurement "$temp" --units m°C
done
